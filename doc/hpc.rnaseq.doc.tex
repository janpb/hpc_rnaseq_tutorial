\documentclass[a4paper, 11pt]{article}
\usepackage{libertine}
\usepackage[scaled=0.85]{DejaVuSansMono}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lejosh}
\usepackage{lejosh.tbl}
\usepackage{lejosh.bib}
\usepackage[english]{babel}
\usepackage{listings}
  \lstset{basicstyle=\footnotesize\ttfamily,
          breaklines=true,
          numbers=left,
          numberstyle=\tiny,
          morecomment=[l][\itshape]{//},
          escapechar=!
          }
\usepackage{dirtree}
  \renewcommand*\DTstylecomment{\rmfamily\itshape}

  \hypersetup{
    pdfauthor={jpb},
    pdftitle={RNA sequence alignment and gene expression analysis on Artemis},
    colorlinks=true,
    linkcolor=black,
    citecolor=blue,
    urlcolor=blue}

\newcommand{\refdir}{$HOME/rnaseq\_tutorial/reference}
\newcommand{\rnadir}{$HOME/rnaseq\_tutorial/rna\_reads}
\newcommand{\workdir}{\$HOME/rnaseq\_tutorial}

\reversemarginpar
\definecolor{lightgray}{gray}{.90}

\newcolumntype{D}{>{\columncolor{white}[\tabcolsep][0pt]}l} %right border l align
\newcolumntype{R}{>{\columncolor{white}[\tabcolsep][0pt]}r} %right border r align
\newcolumntype{L}{>{\columncolor{white}[0pt][\tabcolsep]}l} %left border l align
\newcolumntype{P}{>{\columncolor{white}[0pt][\tabcolsep]}r} %left border r align
\newcolumntype{I}{>{\itshape}l}



\begin{document}
  \title{RNA sequence alignment and gene expression analysis on Artemis}
  \author{Jan P Buchmann  \\
          \texttt{jan.buchmann@sydney.edu.au}}
  \date{2015-08-31}
  \maketitle
  \begin{abstract}
    This tutorial  will show you the first steps in a gene expression analysis
    using RNA-seq data on Artemis. It will cover how to map RNA reads to a
    reference genome using \texttt{tophat} and how to prepare such alignments
    for further analysis using \texttt{cufflinks}. After this tutorial you
    should be able to run \tool{tophat} and \tool{cufflinks} on Artemis.

    RNA sequence has gained a lot of traction with the emergence of cheap(er)
    and high-throughput sequence technologies. RNA-seq data can be used in
    numerous methods, the most common being the investigation of gene
    expression, finding new genes or isoforms, assembling new  genomes.
    RNA-seq analysis uses and creates large data sets, usually in the range of
    several gigabases. This requires the use of automation using computers.
    The bigger, the better.

    The most up-to-date version of this document, slides and example PSB files
    can be found at \url{https://github.com/janpb/hpc_rnaseq_tutorial.git}.
  \end{abstract}

\tableofcontents

  \section{Introduction}
    It is a shortened and Artemis-adjusted version of a previously published
    protocol with more in-depth information by the authors of the Tuxedo tools
    \citep{Trapnell2012}.  It will not cover how to obtain RNA-seq data nor how
    to  interpret it since this is out of scope and whole topic in itself.
    However, some information and resources covering such topics can be found
    in the appendix and references.

    \subsection{The Tuxedo tools}
      The programs used in this tutorial are all part of the Tuxedo tools
      (Table ~\ref{tab:usedTools}). This is  a collection of single
      programs. Each program is very good in doing one specific task, e.g.
      align reads or assemble transcripts. The  real power comes forth by
      concatenating the programs into pipelines where the output from
      the previous program serves as input for the next program
      (Figure~\ref{fig:tuxedoWorkflow}).

      \input{tbls/hpc.course.tuxedosuit.tbl.tex}
      \begin{sidewaysfigure}
        \includegraphics[width=\linewidth]{figs/tuxedo_workflow.pdf}
        \caption{Overview of the Tuxedo tools tutorial workflow.
                \label{fig:tuxedoWorkflow}}
      \end{sidewaysfigure}


      In this tutorial you will perform the individual steps interactively to see
      what is going on under the hood. An overview of the steps in the pipeline
      used in this tutorial is given below:
        \begin{enumerate}
          \item \tool{bowtie2}:   Create index
          \item \tool{tophat}:    Align RNA reads to the reference
          \item \tool{cufflinks}: Assemble expressed genes and transcripts
          \item \tool{cuffmerge}: Create merged transcriptome annotation
          \item \tool{cuffdiff}:  Identifying differentially expressed genes and
                                  transcripts
        \end{enumerate}

      A list of files required for a RNA-seq analysis using the Tuxedo tools is
      given in Table~\ref{tab:tuxedoReqFiles}.
      \input{tbls/tuxedoReqFiles.tbl.tex}


      You will see that two  programs exist, \tool{bowtie} and
      \tool{bowtie2}. While \tool{bowtie2} is the successor of \tool{bowtie},
      \marginpar{\footnotesize{\tool{bowtie} and \tool{bowtie2}}} they cannot be
      used intermixed since \tool{bowtie2} use some different methods, e.g.
      indexes from \tool{bowtie} are not compatible with \tool{bowtie2}.

    \subsection{\tool{samtools}: An useful utility for SAM/BAM file
                manipulation}
      While \tool{samtools} is not part of the Tuxedo tools, it is widely used
      to manipulate files using the SAM and BAM sequence format
      (\citealt{Li2009a}, see Appendix). SAM is a sequence alignment/mapping
      format using TAB-delimited  fields to describe mapping results in ASCII
      format. Since SAM is in ASCII, it can be opened by text editors and used
      in pipelines handling ASCII files. BAM is binary  equivalent of the SAM
      format and consist of blocks of compressed data (think of zip
      archives in a zip archive) and is therefore  mostly used by programs.

    \subsection{The tutorial data set}
      This tutorial uses a subset of public available RNA-seq data from a study
      analyzing the gene expression of Mimivirus \citep{Claverie2009} infecting
      \textit{Acanthamoeba castellanii} (an amoeba, \citealt{Legendre2010}).
      The expression data is available from NCBI's Sequence Read Archive (SRA)
      (\url{http://www.ncbi.nlm.nih.gov/sra}) under the BioProject accession
      \href{http://www.ncbi.nlm.nih.gov/bioproject/PRJNA79545}{PRJNA79545}
      (Table~\ref{tab:rnasample}). The Mimivirus genome can be found in the NCBI
      reference sequence database under the accession
      \href{http://www.ncbi.nlm.nih.gov/nuccore/311977355/}{NC\_014649} at
      NCBI's refseq database (\url{http://www.ncbi.nlm.nih.gov/refseq/}).
      While this example is very small and uses only two conditions, you will
      encounter bigger libraries with several conditions.

      \input{tbls/hpc.course.rnasample.tbl.tex}

      RNA-seq data comes in different flavors. In this tutorial
      we will use RNA-seq data where a primer on the 5\p-end was used to
      generate the read. Other RNA-seq libraries use paired-end reads or can
      have strand specific reads. Such data sets are analyzed using the same
      procedure, but the commands for the individual steps have to be adjusted.
      Therefore, check what type of data you have and read the man pages how to
      adjust the corresponding commands.

      For this tutorial, we will use an interactive Artemis session to start the
      programs. While this is not the proper way to do such an analysis,
      manually preparing your working directory and invoking the separate
      commands will give you an idea what goes on in the background if you
      would run this using a batch file.

\clearpage

  \section{Tutorial: Analyzing Mimivirus gene expression}

    The tutorial has following steps:
    \begin{enumerate}
        \item Get the required data and set up your initial directory structure.
        \item Start an interactive Artemis session for the analysis.
        \item Align the RNA-seq reads to the reference genome
        \item Assemble expressed genes and transcripts
        \item Identify differentially expressed genes and transcripts
    \end{enumerate}


    \subsection{Preparing the working directory}
      The initial step in your analysis is to prepare your working directory.
      It is considered best practice to create one directory for each analysis
      with sub-directories containing the original data and results.
      \begin{itemize}
        \item The Tuxedo tools create several files as output. If you run
              \tool{tophat} on two  data sets in the same directory it will
              overwrite the previous results.

        \item You will start with numerous files and create more during the
              analysis. Trying to keep track of your progress, analyzing data
              and solving errors is easier by using  a logic directory
              structure.

        \item You will/should use automated approaches for larger projects
              (that's why you use computers).  Good directory structures help
              keeping everything separated as well as avoiding confusion and
              overwriting files.
      \end{itemize}

      Set up your initial directory structure as described in
      Listing~\ref{lst:initDirSetup}.
      \input{lst/initDirSetup.lst.tex}

      Copy the Mimivirus genome and annotations as well as the RNA-seq
      libraries into the corresponding directories
      (Table~\ref{tab:reqFilesPath}).
      \input{tbls/reqFilesPaths.tbl.tex}\\

      You should have the initial directory structure ready
      (Figure~\ref{fig:initDirStruc}) before proceeding to the next step.
      \input{figs/initDirStruc.fig.tex}

\clearpage

    \subsection{Loading the required modules on Artemis}
      After preparing your working directory, start your interactive Artemis
      session (Listing~\ref{lst:initHPC}).
      \input{lst/initHPC.lst.tex}

      For a RNA-seq analysis using the Tuxedo tools on Artemis, we need to load
      following modules: \texttt{samtools}, \texttt{bowtie2}, \texttt{tophat},
      \texttt{cufflinks}. The commands are shown in
      Listing~\ref{lst:HPCmodload}.
      \input{lst/hpc.modload.lst.tex}

      With a proper directory structure and running Artemis session you can
      begin to align the RNA-seq data.

    \subsection{\tool{tophat}: Aligning RNA-seq reads to a reference genome}
      The first step in almost all RNA-seq analysis is the alignment of RNA
      reads obtained from a sequencing run to a reference genome. We will
      \tool{tophat} which uses the sequence alignment engine from
      \tool{bowtie2}. In contrast to \tool{bowtie2}, \tool{tophat} allows
      longer gaps in the alignments and is therefore ideal to map RNA reads
      over introns. The use of \tool{bowtie2} as engine requires the creation
      of an index for the reference genome.

    \subsubsection{Creating the \tool{bowtie2} index}
      The command to create the \tool{bowtie2} index is shown in
      Listing~\ref{lst:mkBt2Idx}.
      \input{lst/mkBt2Idx.lst.tex}

      You will see some output while running \tool{bowtie2-build}. If
      everything goes as expected, you should have the index files
      (\texttt{*.bt2} files) in your reference directory
      (\url{\refdir}, Figure~\ref{fig:idxDirStruc}).
      \input{figs/idxDirStruc.fig.tex}

    \subsubsection{Mapping RNA-seq data}
      With the \tool{bowtie2} index ready, you can now align the RNA-seq reads
      to the reference genome using tophat as shown in
      Listing~\ref{lst:runth_se}.
      \input{lst/runTH.lst.tex}
      After the successful \tool{tophat}, your directory should look like
      Figure~\ref{fig:thDirStruc}.
      \input{figs/thDirStruc.fig.tex}

\clearpage

    \subsection{\tool{cufflinks}: Assembling expressed genes and transcripts}
      In the previous step you aligned the RNA reads to the reference genome
      using \tool{tophat}. Now you need to run \tool{Cufflinks} to assemble
      transcripts, estimate their abundances, and test for  differences in
      expression and regulation in the RNA-Seq samples. \tool{cufflinks} needs
      the results from the \tool{tophat} run (\texttt{accepted\_hits.bam}) as
      argument. However, it has several optional parameters to adjust abundance
      estimates and assemble the transcripts, e.g. specifying maximum intron
      lengths. The commands to invoke \tool{cufflinks} on the earlier
      \tool{tophat} results are shown in Listing~\ref{lst:runcl}.
      \input{lst/runCL.lst.tex}

      The results of the \tool{cufflinks} run are shown in
      Figure~\ref{fig:clDirStruc}.
      \input{figs/clDirStruc.fig.tex}

\clearpage

      \subsubsection{\tool{cuffmerge}: Merge transcriptome assemblies}
        When analyzing different RNA-seq libraries with newly assembled
        transcriptomes, a "master" transcriptome needs to be created.
        The main  purpose of \tool{cuffmerge} is to assemble a
        transcript/annotation file  (\texttt{GTF} file) which is required by
        \tool{cuffdiff}. \tool{cuffmerge} runs  \tool{cuffcompare} and
        automatically filters a number of transfrags (short transcribed
        fragments)  that are probably artifacts. Using an annotation file will
        merge novel isoforms and known isoforms as well as maximize overall
        assembly quality.

        \tool{cuffmerge} needs a file containing the paths for each assembled
        transcript  from \tool{cufflinks}, one path per line
        (Listing~\ref{lst:asmFile}.). We will call this file
        \texttt{assemblies}.
        \input{lst/asmFile.lst.tex}

        Creating \texttt{assemblies} can be done by opening an empty file and
        write/copy each file path by  hand. However, this is senseless,
        especially when dealing with a real data set. The smart way is to use
        the  command line to create \texttt{assemblies}
        (Figure~\ref{lst:mkAsmFile}).
        \input{lst/mkAsmFile.lst.tex}

        \tool{cuffmerge} uses the newly created file \texttt{assemblies} as
        argument to create the merged transcription data. This is required for
        the expression level  analysis between the RNA samples.
        \tool{cuffmerge} is invoked as shown in Listing~\ref{lst:runCM}.
        \input{lst/runCM.lst.tex}

        The results of the \tool{cuffmerge} run are shown in
        Figure~\ref{fig:cmDirs}.
        \input{figs/cmDirStruc.fig.tex}

\clearpage

    \subsection{\tool{cuffdiff}: Identifying differentially expressed gene and
                transcripts}
      Comparing expression levels of genes and transcripts in RNA-Seq
      experiments is a hard problem. Cuffdiff is a highly accurate tool to
      perform expression level comparisons of genes and transcripts. It can
      identify which genes are up- or down-regulated between two or more
      conditions and which genes are differently spliced or are undergoing
      other types of isoform-level regulation. Its results can be than further
      analyzed using \tool{cummeRbund} \citep{Goff2013}, a \tool{R} package for
      the visualization of  RNA-Seq analysis results.

      While \tool{cuffdiff} requires only the transcripts and mapped reads for
      each library as arguments, we can use the \texttt{-u} option to increase
      the accuracy of the expression analysis. The \texttt{-u} options takes
      the path to the \tool{cuffmerge} output directory as parameter
      (Listing~\ref{lst:runCD}).
      \input{lst/runCD.lst.tex}

      \tool{cuffdiff} creates numerous output files (Figure~\ref{fig:cdDirs}).
      For this tutorial we are only interested in the files ending in
      \texttt{*.diff} found in the \tool{cuffdiff} output directory. They
      indicate if any genes, transcripts, isoforms or promoters have a
      significant difference in their expression between the two analyzed
      RNA-seq libraries.
      \input{figs/cdDirStruc.fig.tex}

      The next step in your analysis will be to further look into the results
      to double-check if some results actually make sense, e.g. true replicates
      should return only few or no changes in expression level while different
      tissues or conditions are more likely to have hundred of differentially
      expressed genes. While you can do it by using own tools, there is an
      \tool{R} package
      \href{http://compbio.mit.edu/cummeRbund/index.html}{cummeRbund}
      which processes the \tool{cuffdiff} outputs and simplifies the subsequent
      analysis, which is actually the tricky part of an RNA-seq  analysis.

      However, we can check if there is an indication for differentially
      expressed genes or transcripts in our analysis. The \texttt{*.diff} files
      are TAB-delimited files which can be parsed easily. The last field of a
      \texttt{*.diff} files indicates if the measured expression levels
      differed significantly between the analyzed data set, either by 'yes' or
      'no'.  Using \tool{grep} (a common utility tool to look for patterns in
      lines on *NIX systems) we can make a quick and dirty check
      (Listing~\ref{lst:chkDiff}). We print each line in the \texttt{*.diff}
      files which have the word 'yes' and pipe it into \texttt{less} (a common
      terminal pager program aka a file viewer).

      \citet{Trapnell2012} published an excellent protocol which explains how
      to use \tool{tophat} \tool{cummeRbund} in deeper detail. I highly
      recommend it if you want to begin your first RNA-seq analysis.
\newpage
      \input{lst/chkDiff.lst.tex}
      \input{lst/diffResult.lst.tex}

\clearpage
  \section{Appendix}
    \subsection{General tips}
      If you run into problems or getting stuck, try some of the following.

      \paragraph{Create a minimal working example}
      Create a fake data set with few entries. This helps you to check if there
      is a problem with your data or with the program. See below.

      \paragraph{Run the examples and tutorials}
      Several programs have an example data-set for you to run and test if
      everything works as expected. Sometimes installations go wrong or you
      missed a dependency. If the program came with an example, run it first to
      see if it actually  works. If there are no examples and you can access
      the source of the program (e.g. through GitHub), look for a directory
      called example, tutorial, test or similar.

      \paragraph{Read the manual}
      The manuals of most programs can be invoked by the command
      \tool{man}, e.g. \texttt{man tophat}. This (hopefully) calls the manual
      of the program with the options and parameters  explained. At the top of
      the man page you find usually an usage (a synopsis of the command) while
      examples at the end.

      \paragraph{Sometimes, there is no manual}
      Try to find the help. The help of a program is a compact version of
      the manual and is mostly invoked using following parameters: \texttt{-h,
      H, -?} or just run the program without any arguments or parameters, e.g.
      \texttt{tophat -h} or \texttt{tophat}. The help lists all needed
      parameters and options without deeper explanations.  You can quickly
      check if you invoke the program correctly, if you are using wrong
      parameters or to remember a parameter.

      \paragraph{Sometimes, there is no usage}
      Or the manual is outdated. Search the Internet for similar problems. If
      you encounter an error message, use this message as query. It's very
      likely you are not the first one encountering the issue.

      If you are the first one and can reproduce it, write the developers or
      file bug report. They are (officially) happy if somebody points to their
      mistakes and errors.

    \subsection{File formats}
      You will encounter several file formats along the way and sometimes you
      have to convert one into an other. There are several tools in the wild,
      but due  to changing formats and their complexity they are not always
      up-to-date. Certain programs are very picky and will refuse to
      read certain formats if there are characters present they do not like,
      e.g. empty lines or different newlines etc. A common issue is the newline
      character, which differs in Windows, Mac(prior to Mac OSX) and Linux, with
      the ability to cause hilarity  during the analysis. If you have  data
      prepared in Windows and want to  use it on Linux, there is a program
      \tool{dos2unix} which will convert newlines from Windows to Linux and
      vice-versa.

    \subsection{Weblinks}
    \input{tbls/rnaseqTuts.tbl.tex}
    \input{tbls/fmtConvLnks.tbl.tex}
    \input{tbls/fmtSpecLnks.tbl.tex}
    \input{tbls/toolLnks.tbl.tex}

\clearpage
  \bibliographystyle{bib/tpj}
  \bibliography{bib/refs.bib}
\end{document}
