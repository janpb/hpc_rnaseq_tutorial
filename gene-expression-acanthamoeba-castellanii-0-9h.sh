#!/bin/bash
#-----------------------------
#gene-expression-acanthamoeba-castellanii-0-9h.sh
#(c) J.P.Buchmann 
#----------------------------
#we are in top directory
SIGNATURE=.518dd69f-2039-4afd-bbcf-6fa5bb66a993
[[ ! -e ${SIGNATURE} ]] && { echo "you are not in top directory"; exit 1; }
cd work
QSUB=sh
QSUB=qsub
PBS_O_WORKDIR=$(pwd) ${QSUB} ../pbs/rnaseq_tuxedo.psb
