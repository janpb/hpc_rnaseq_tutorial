HPC rnaseq tutorial - Analyzing RNA-seq data on Artemis (HPC USYD)
========
This is a tutorial showing how to use Tuxedo tools (bowtie2, tophat, cufflinks,
see [0] and links therein) for a RNA-seq analysis on Artemis, the HPC from the
University of Sydney. It focuses on invoking the required tools and not on the
RNA-seq analysis itself. It is intended for users which have basic knowledge of
a *NIX system. You should be able to use the command line for basic tasks, e.g.
copying and moving files. You should have an idea about a *NIX filesystem, as
well. If you know what relative and absolute paths, $HOME directory and working
directory mean, go ahead. Otherwise, read up on this because you will use them
when working on a *NIX system, e.g. on a HPC.

User with command line and HPC experience should use the protocol at [1]. It is
more detailed and will suit you way better.

Required Files
--------------
- RNA-seq data (fetch from [2])
- Mimivirus genome (fetch from [3])
- Mimivirus annotation (in repo)

Please let me know if you find any errors.

Enjoy,

jpb


References
----------

- [0] http://ccb.jhu.edu/software/tophat/index.shtml
- [1] http://www.nature.com/nprot/journal/v7/n3/full/nprot.2012.016.html
- [2] http://www.ncbi.nlm.nih.gov/bioproject/PRJNA79545
- [3] http://www.ncbi.nlm.nih.gov/nuccore/311977355/
